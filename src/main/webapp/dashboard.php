<?php
session_start();
if (isset($_SESSION["username"])) {
    $username = $_SESSION["username"];
    session_write_close();
} else {
    // since the username is not set in session, the user is not-logged-in
    // he is trying to access this page unauthorized
    // so let's clear all session variables and redirect him to index
    session_unset();
    session_write_close();
    $url = "./index.php";
    header("Location: $url");
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Welcome to dashboard</title>
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
		<script src="vendor/jquery/jquery-3.3.1.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="header">
			<h1>Registo</h1>
			<nav>
				<ul>
					<li><a href="index.php">Logout</a></li>
				</ul>
			</nav>
		</div>
		<div style="clear:both"></div>
		<div class="container">
			<div class="block">
				<div style="margin-top: 20%;">
					Welcome <?php echo $username;?>
					<h2>Thank you for visiting our dashboard, for more devops references <a href="https://devoprefs.blogspot.com/">devoprefs.blogspot.com</a></h2>
				</div>
			</div>
		</div>
		<div style="clear:both"></div>
		<div class="footer">
			<p>&copy; 2020 Registo. Designed & Developed by Surya Rall</p>
		</div>
	</body>
</html>