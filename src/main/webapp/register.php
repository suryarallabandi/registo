<?php
use Phppot\Member;
if (! empty($_POST["signup-btn"])) {
    require_once './Model/Member.php';
    $member = new Member();
    $registrationResponse = $member->registerMember();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Regsiter for dashboard</title>
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
		<script src="vendor/jquery/jquery-3.3.1.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="header">
			<a href="index.php"><h1>Registo</h1></a>
		</div>
		<div style="clear:both"></div>
		<div class="container">
			<div class="block">
				<div class="align">
					<form name="sign-up" action="" method="post" onsubmit="return signupValidation()">
						<?php
							if (! empty($registrationResponse["status"])) {
						?>
						<?php
							if ($registrationResponse["status"] == "error") {
						?>
						<div class="server-response error-msg"><?php echo $registrationResponse["message"]; ?></div>
						<?php 
							} else if ($registrationResponse["status"] == "success") { 
						?>
						<div class="server-response success-msg"><?php echo $registrationResponse["message"]; ?></div>
						<?php
				        	}
				        ?>
						<?php
				    		}
				    	?>
				    	<div class="error-msg" id="error-msg"></div>
				    	<span class="required error" id="username-info"></span>
						<input class="input-box-330" type="text" name="username" id="username" placeholder="Username">
						<span class="required error" id="email-info">
						<input class="input-box-330" type="email" name="email" id="email" placeholder="Email Address">
						<span class="required error" id="signup-password-info">
						<input class="input-box-330" type="password" name="signup-password" id="signup-password" placeholder="Password">
						<span class="required error" id="confirm-password-info"></span>
						<input class="input-box-330" type="password" name="confirm-password" id="confirm-password" placeholder="Confirm Password">
						<input class="btn" type="submit" name="signup-btn" id="signup-btn" value="SignUp">
					</form>
				</div>
				<br>
				<p style="text-align: center;">Already registered</p>
				<br>
				<a href="login.php"><input type="submit" name="submit" id="submit" value="Login"></a>
			</div>
		</div>
		<div style="clear:both"></div>
		<div class="footer">
			<p>&copy; 2020 Registo. Designed & Developed by Surya Rall</p>
		</div>
		<script>
			function signupValidation() {
				var valid = true;

				$("#username").removeClass("error-field");
				$("#email").removeClass("error-field");
				$("#password").removeClass("error-field");
				$("#confirm-password").removeClass("error-field");

				var UserName = $("#username").val();
				var email = $("#email").val();
				var Password = $('#signup-password').val();
			    var ConfirmPassword = $('#confirm-password').val();
				var emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

				$("#username-info").html("").hide();
				$("#email-info").html("").hide();

				if (UserName.trim() == "") {
					$("#username-info").html("required.").css("color", "#ee0000").show();
					$("#username").addClass("error-field");
					valid = false;
				}
				if (email == "") {
					$("#email-info").html("required").css("color", "#ee0000").show();
					$("#email").addClass("error-field");
					valid = false;
				} else if (email.trim() == "") {
					$("#email-info").html("Invalid email address.").css("color", "#ee0000").show();
					$("#email").addClass("error-field");
					valid = false;
				} else if (!emailRegex.test(email)) {
					$("#email-info").html("Invalid email address.").css("color", "#ee0000")
							.show();
					$("#email").addClass("error-field");
					valid = false;
				}
				if (Password.trim() == "") {
					$("#signup-password-info").html("required.").css("color", "#ee0000").show();
					$("#signup-password").addClass("error-field");
					valid = false;
				}
				if (ConfirmPassword.trim() == "") {
					$("#confirm-password-info").html("required.").css("color", "#ee0000").show();
					$("#confirm-password").addClass("error-field");
					valid = false;
				}
				if(Password != ConfirmPassword){
			        $("#error-msg").html("Both passwords must be same.").show();
			        valid=false;
			    }
				if (valid == false) {
					$('.error-field').first().focus();
					valid = false;
				}
				return valid;
			}
		</script>
	</body>
</html>