<!DOCTYPE html>
<html>
	<head>
		<title>Sample user registration for devops project</title>
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
		<script src="vendor/jquery/jquery-3.3.1.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="header">
			<a href="index.php"><h1>Registo</h1></a>
		</div>
		<div style="clear:both"></div>
		<div class="container">
			<div class="block">
				<div class="signup">
					<a href="register.php"><button>SignUp</button></a>
				</div>
				<div class="login">
					<a href="login.php"><button>Login</button></a>
				</div>
			</div>
		</div>
		<div style="clear:both"></div>
		<div class="footer">
			<p>&copy; 2020 Registo. Designed & Developed by Surya Rall</p>
		</div>
	</body>
</html>