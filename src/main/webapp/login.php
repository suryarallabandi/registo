<?php
use Phppot\Member;

if (! empty($_POST["login-btn"])) {
    require_once __DIR__ . '/Model/Member.php';
    $member = new Member();
    $loginResult = $member->loginMember();
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Dashboard Login</title>
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
		<script src="vendor/jquery/jquery-3.3.1.js" type="text/javascript"></script>
	</head>
	<body>
		<div class="header">
			<a href="index.php"><h1>Registo</h1></a>
		</div>
		<div style="clear:both"></div>
		<div class="container">
			<div class="block">
				<div class="align">
					<form name="login" action="" method="post" onsubmit="return loginValidation()">
						<?php if(!empty($loginResult)){?>
						<div class="error-msg"><?php echo $loginResult;?></div>
						<?php }?>
						<span class="required error" id="username-info"></span>
						<input class="input-box-330" type="text" name="username" id="username" placeholder="Username">
						<span class="required error" id="login-password-info">
						<input class="input-box-330" type="password" name="login-password" id="login-password" placeholder="Password">
						<input class="btn" type="submit" name="login-btn" id="login-btn" value="Login">
					</form>
				</div>
				<br>
				<p style="text-align: center;">Not regsitered yet, signup now</p>
				<br>
				<a href="register.php"><input type="submit" name="submit" id="submit" value="SignUp"></a>
			</div>
		</div>
		<div style="clear:both"></div>
		<div class="footer">
			<p>&copy; 2020 Registo. Designed & Developed by Surya Rall</p>
		</div>
		<script>
			function loginValidation() {
				var valid = true;
				$("#username").removeClass("error-field");
				$("#password").removeClass("error-field");

				var UserName = $("#username").val();
				var Password = $('#login-password').val();

				$("#username-info").html("").hide();

				if (UserName.trim() == "") {
					$("#username-info").html("required.").css("color", "#ee0000").show();
					$("#username").addClass("error-field");
					valid = false;
				}
				if (Password.trim() == "") {
					$("#login-password-info").html("required.").css("color", "#ee0000").show();
					$("#login-password").addClass("error-field");
					valid = false;
				}
				if (valid == false) {
					$('.error-field').first().focus();
					valid = false;
				}
				return valid;
			}
		</script>
	</body>
</html>